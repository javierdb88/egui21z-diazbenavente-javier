#include "activitydialog.h"
#include "ui_activitydialog.h"
#include <QFileDialog>
#include <QFile>
#include <QJsonDocument>
#include <QTextStream>
#include <QJsonObject>
#include <QJsonValue>
#include <QMessageBox>
#include <QJsonArray>
#include <QJsonValueRef>
#include <limits>
#include <QtDebug>


QString file_entry = QDir::currentPath() + "/entry.json";;
QString file_act2;


void ActivityDialog::set_file_act(QString file) {

    file_act2 = file;
}



QJsonDocument ActivityDialog::readJson(QString filePath) {                              // reads a json document
    QFile jsonFile(filePath);
    jsonFile.open(QIODevice::ReadWrite | QIODevice::Text);
    return QJsonDocument::fromJson(jsonFile.readAll());
}


void ActivityDialog::saveJson(QJsonDocument doc, QString filePath) {                    // saves a json document
    QFile jsonFile(filePath);
    jsonFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QByteArray data_json = doc.toJson();
    QFile output(filePath);
    if (output.open(QIODevice::WriteOnly | QIODevice::Text)) {
        output.write(data_json);
        output.close();
        QMessageBox::information(this, tr("Saved"), tr("Document saved succesfully"));
    } else {
        QMessageBox::critical(this, tr("Error"), output.errorString());
    }

}



ActivityDialog::ActivityDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ActivityDialog)
{
    ui->setupUi(this);

    ui->time_spinBox->setRange(0, std::numeric_limits<int>::max());                               // numerical limit for time

    QJsonDocument doc = readJson(file_act2);
    QStringList codes = find_attributes_activities(doc, "Code");


    ui->code_combobox->addItems(codes);

    QStringList subcodes = find_subcode_code(doc, ui->subcode_combobox->currentIndex());

    ui->subcode_combobox->addItems(subcodes);

}


QStringList ActivityDialog::find_attributes_activities(QJsonDocument doc, QString attribute){       // finds all the values for an attribute (every code/manager/budget...)
    QJsonObject obj = doc.object();
    QStringList array;
    QJsonValueRef ref = obj.find("activities").value();
    QJsonArray ref_array = ref.toArray();
    for (int i = 0; i < ref_array.size(); i++) {
        QJsonObject index_obj = ref_array[i].toObject();
        QJsonValueRef ref_attr = index_obj.find(attribute).value();
        array.append(ref_attr.toString());
    }
    return array;
}


QStringList ActivityDialog::find_subcodes(QJsonDocument doc) {              // finds all the subcodes and returns a list that contains them

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find("activities").value();

    QJsonArray ref_array = ref.toArray();

    QStringList subcodes;

    for (int i = 0; i < ref_array.size(); i++) {
        QJsonObject index_obj = ref_array[i].toObject();
        QJsonValueRef ref_subactivities = index_obj.find("Subactivities").value();
        QJsonArray ref_subactivities_array = ref_subactivities.toArray();
        for (int j = 0; j < ref_subactivities_array.size(); j++) {
            QJsonObject subindex_obj = ref_subactivities_array[j].toObject();
            QJsonValueRef ref_subcode = subindex_obj.find("code").value();
            subcodes.append(ref_subcode.toString());

        }


    }

    return subcodes;
}

QStringList ActivityDialog::find_subcode_code(QJsonDocument doc, int index) {        // finds the subcodes for a given index of an object and returns a list that contains them

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find("activities").value();

    QJsonArray ref_array = ref.toArray();

    QStringList subcodes;
    QJsonObject index_obj = ref_array[index].toObject();
    QJsonValueRef ref_subactivities = index_obj.find("Subactivities").value();
    QJsonArray ref_subactivities_array = ref_subactivities.toArray();

    for (int j = 0; j < ref_subactivities_array.size(); j++) {
        QJsonObject subindex_obj = ref_subactivities_array[j].toObject();
        QJsonValueRef ref_subcode = subindex_obj.find("code").value();
        subcodes.append(ref_subcode.toString());

    }



    return subcodes;
}


QString ActivityDialog::find_attribute_index(QJsonDocument doc, int index, QString attr) {        // finds the value of a certain atribute in the json, given its index (and the key)

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find("activities").value();

    QJsonArray ref_array = ref.toArray();

    QJsonObject index_obj = ref_array[index].toObject();
    QJsonValueRef ref_attr = index_obj.find(attr).value();
    return ref_attr.toString();

}


int ActivityDialog::find_attribute_index_int(QJsonDocument doc, int index, QString attr, QString filename) {        // finds the value of a certain atribute in the json, given its index (and the key)

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find(filename).value();

    QJsonArray ref_array = ref.toArray();

    QJsonObject index_obj = ref_array[index].toObject();
    QJsonValueRef ref_attr = index_obj.find(attr).value();
    return ref_attr.toInt();
}

QString ActivityDialog::find_attribute_index_entry(QJsonDocument doc, int index, QString attr) {        // finds the value of a certain atribute in the json, given its index (and the key)

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find("entries").value();

    QJsonArray ref_array = ref.toArray();

    QJsonObject index_obj = ref_array[index].toObject();
    QJsonValueRef ref_attr = index_obj.find(attr).value();
    return ref_attr.toString();

}

int ActivityDialog::get_index(QJsonDocument doc, QString attr, QString attr_value) {      // returns the index of an object in the 'activities' array, given a certain attribute and its value

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find("activities").value();

    QJsonArray ref_array = ref.toArray();

    for (int i = 0; i < ref_array.size(); i++) {

        QJsonObject index_obj = ref_array[i].toObject();
        QJsonValueRef ref_attr = index_obj.find(attr).value();

        if (ref_attr.toString() == attr_value) {                                            // if the value value found is the same as the parameter 'attr_value', we return the index
            return i;
        }

    }
    return -1;                                                                              // if the index is not found, we return -1
}



int ActivityDialog::get_index_entry(QJsonDocument doc, QString attr, QString attr_value) {      // returns the index of an object in the 'activities' array, given a certain attribute and its value

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find("entries").value();

    QJsonArray ref_array = ref.toArray();

    for (int i = 0; i < ref_array.size(); i++) {

        QJsonObject index_obj = ref_array[i].toObject();
        QJsonValueRef ref_attr = index_obj.find(attr).value();
        if (ref_attr.toString() == attr_value) {                                            // if the value value found is the same as the parameter 'attr_value', we return the index
            return i;
        }

    }
    return -1;                                                                              // if the index is not found, we return -1
}


int ActivityDialog::get_index_int(QString file_name, QJsonDocument doc, QString attr, int attr_value) {      // returns the index of an object in the 'activities' array, given a certain attribute and its value

    QJsonObject obj = doc.object();

    QJsonValueRef ref = obj.find(file_name).value();

    QJsonArray ref_array = ref.toArray();

    for (int i = 0; i < ref_array.size(); i++) {

        QJsonObject index_obj = ref_array[i].toObject();
        QJsonValueRef ref_attr = index_obj.find(attr).value();

        if (ref_attr.toInt() == attr_value) {                                            // if the value value found is the same as the parameter 'attr_value', we return the index

            return i;
        }

    }
    return -1;                                                                              // if the index is not found, we return -1
}


ActivityDialog::~ActivityDialog()
{
    delete ui;
}


void ActivityDialog::on_cancel_button_clicked()
{
    close();
}




void ActivityDialog::on_ok_button_clicked()
{


        QDate date = ui->date_dateedit->date();
        QString code = ui->code_combobox->currentText();
        QString subcode = ui->subcode_combobox->currentText();
        const int time = ui->time_spinBox->value();
        QString description = ui->description_lineedit->text();

        QJsonDocument doc = readJson(file_entry);
        QJsonDocument doc2 = readJson(file_act2);

        int code_index = get_index(doc2, "Code", code);                          // we get the index for the object that contains the code selected
        QString active = find_attribute_index(doc2, code_index, "Active");       // we obtain the value for 'Active' in that project
        qDebug() << get_index_entry(doc, "subcode", subcode);
        if (get_index_entry(doc, "subcode", subcode) != -1) {                          // user cannot store two activities with the same subcode

            QMessageBox::warning(this, "Warning", "Activity with that subcode already exists");
        }

        else if (active == "false" && time != 0) {                                   // the user cannot assign time to a project if it is not active, so if it is not 0, we display a message

            QMessageBox::warning(this, "Warning", "You cannot assign time to a project that is not active. Therefore, time should be equal to 0");
        }

        else {

            QJsonObject obj = doc.object();
            QJsonObject obj2;

            QJsonValueRef ref = obj.find("entries").value();
            QJsonArray entries = ref.toArray();
            QJsonArray entries_copy = entries;

            obj2["date"] = date.toString("yyyy-MM-dd");
            obj2["Code"] = code;
            obj2["subcode"] = subcode;
            obj2["time"] = time;
            obj2["description"] = description;


            entries_copy.append(obj2);



            obj["entries"] = entries_copy;

            doc.setObject(obj);

            QByteArray data_json = doc.toJson();
            QFile output(file_entry);
            if (output.open(QIODevice::WriteOnly | QIODevice::Text)) {
                output.write(data_json);
                output.close();
                QMessageBox::information(this, tr("Saved"), tr("Document saved succesfully"));
            } else {
                QMessageBox::critical(this, tr("Error"), output.errorString());
            }

            close();

        }
}



void ActivityDialog::on_code_combobox_currentIndexChanged(int index)
{
    QJsonDocument doc = readJson(file_act2);
    QStringList subcodes = find_subcode_code(doc, index);
    ui->subcode_combobox->clear();
    ui->subcode_combobox->addItems(subcodes);


}

