#ifndef ACTIVITYDIALOG_H
#define ACTIVITYDIALOG_H

#include <QDialog>

namespace Ui {
class ActivityDialog;
}

class ActivityDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ActivityDialog(QWidget *parent = nullptr);
    ~ActivityDialog();

    QStringList find_attributes_activities(QJsonDocument, QString);

    QStringList find_subcodes(QJsonDocument);

    QStringList find_subcode_code(QJsonDocument, int);

    QString find_attribute_index(QJsonDocument, int, QString);

    int find_attribute_index_int(QJsonDocument, int, QString, QString);

    QString find_attribute_index_entry(QJsonDocument, int, QString);

    int get_index(QJsonDocument, QString, QString);

    int get_index_entry(QJsonDocument, QString, QString);

    int get_index_int(QString, QJsonDocument, QString, int);

    QJsonDocument readJson(QString);

    void saveJson(QJsonDocument, QString);

    void set_file_act(QString);

private slots:
    void on_cancel_button_clicked();

    void on_ok_button_clicked();

    void on_code_combobox_currentIndexChanged(int index);

private:
    Ui::ActivityDialog *ui;
};

#endif // ACTIVITYDIALOG_H
