#ifndef ENTRYDIALOG_H
#define ENTRYDIALOG_H

#include <QDialog>

namespace Ui {
class EntryDialog;
}

class EntryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EntryDialog(QWidget *parent = nullptr);
    ~EntryDialog();

    void set_username_entry(QString);

    QJsonDocument readJson(QString);


private slots:

    void on_ok_button2_clicked();

    void on_cancelButton_clicked();





private:
    Ui::EntryDialog *ui;
};

#endif // ENTRYDIALOG_H
