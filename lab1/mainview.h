#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>


namespace Ui {
class mainView;
}

class mainView : public QDialog
{
    Q_OBJECT

public:
    explicit mainView(QWidget *parent = nullptr);
    ~mainView();

    void set_username(QString);

    void set_file_actMain(QString);

    void modify_attribute_project(QString, QString, QString, QString, QString);

    void modify_int_attribute_project(QString, QString, QString, int, int);

    void modify_subproject(QString, QString, QString, QString, QString, QString);

private slots:

    void on_AddButton_2_clicked();

    void on_add_activity_button_clicked();

    void on_edit_project_button_clicked();

    void on_edit_activity_button_clicked();

    void on_delete_activity_button_clicked();

private:
    Ui::mainView *ui;
};

#endif // MAINVIEW_H
