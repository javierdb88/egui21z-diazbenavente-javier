#include "entrydialog.h"
#include "ui_entrydialog.h"
#include <QFileDialog>
#include <QFile>
#include <QJsonDocument>
#include <QTextStream>
#include <QJsonObject>
#include <QJsonValue>
#include <QMessageBox>
#include <limits>
#include "mainwindow.h"
#include "activitydialog.h"
#include <iostream>
#include <QtDebug>
#include <QJsonArray>
#include <QDir>
#include <QJsonValueRef>



QString user2;

EntryDialog::EntryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntryDialog)
{
    ui->setupUi(this);

    ui->budget_spinbox->setRange(0, std::numeric_limits<int>::max());                               // numerical limit for budget

    QStringList boolValues;
    boolValues.append("true");
    boolValues.append("false");
    ui->active_combobox->addItems(boolValues);                                                      // True and False added as options to active_combobox
    ui->active_combobox->setCurrentIndex(0);

    MainWindow mywindow;

    ui->manager_lineedit->setText(user2);                                                           // the user becomese the manager of the project

}



void EntryDialog::set_username_entry(QString userName)                                              // setter for username
{
    user2 = userName;

}

EntryDialog::~EntryDialog()
{
    delete ui;
}


QJsonDocument EntryDialog::readJson(QString filePath) {                                             // reads a json document
    QFile jsonFile(filePath);
    jsonFile.open(QIODevice::ReadWrite | QIODevice::Text);
    return QJsonDocument::fromJson(jsonFile.readAll());
}

void EntryDialog::on_ok_button2_clicked()                                                           // stores the values selected on a Json document
{



    QString file_name = QDir::currentPath() + "/activity.json";;

    QString code = ui->code_lineedit->text();
    QString name = ui->name_lineedit->text();
    const int budget = ui->budget_spinbox->value();
    QString active = ui->active_combobox->currentText();
    QString subproject = ui->subprojects_lineedit->text();


    ActivityDialog act;


    QJsonDocument doc = readJson(file_name);

    QStringList codes = act.find_attributes_activities(doc, "Code");


    for (int i = 0; i < codes.size(); i++) {

        if (codes[i] == code) {
            QMessageBox::warning(this, "Warning", "You cannot create two projects with the same name");
            return;
        }
    }

    QJsonObject obj = doc.object();
    QJsonObject obj2;
    QJsonObject subproject_obj;

    QJsonValueRef ref = obj.find("activities").value();
    QJsonArray activities = ref.toArray();


    QJsonArray activities_copy = activities;


    obj2["Code"] = code;
    obj2["Manager"] = user2;
    obj2["Name"] = name;
    obj2["Budget"] = budget;
    obj2["Active"] = active;

    QJsonArray subactivities;

    if(subproject != "") {
        subproject_obj["code"] = subproject;


        subactivities.append(subproject_obj);
    }

    obj2["Subactivities"] = subactivities;                                       // subactivities is a list that contains objects

    activities_copy.append(obj2);

    obj["activities"] = activities_copy;

    doc.setObject(obj);

    QByteArray data_json = doc.toJson();
    QFile output(file_name);
    if (output.open(QIODevice::WriteOnly | QIODevice::Text)) {
        output.write(data_json);
        output.close();
        QMessageBox::information(this, tr("Saved"), tr("Document saved succesfully"));
    } else {
        QMessageBox::critical(this, tr("Error"), output.errorString());
    }

    close();
}


void EntryDialog::on_cancelButton_clicked()
{
    close();

}


