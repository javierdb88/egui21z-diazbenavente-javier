#include "subactivitiesdialog.h"
#include "ui_subactivitiesdialog.h"
#include "activitydialog.h"
#include "mainview.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QInputDialog>
#include <QDir>
#include <QDebug>


QString file_act3 = QDir::currentPath() + "/activity.json";;
QString code;



SubactivitiesDialog::SubactivitiesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SubactivitiesDialog)
{
    ui->setupUi(this);

}

void SubactivitiesDialog::set_code(QString code_param) {                        // setter for code

    code = code_param;
    initialize(code);
}


void SubactivitiesDialog::initialize(QString code_param) {                      // value innitialization

    mainView view;
    ActivityDialog act;
    QJsonDocument doc = act.readJson(file_act3);

    QStringList subcodes = act.find_subcode_code(doc, act.get_index(doc, "Code", code_param));
    ui->comboBox->addItems(subcodes);

}

SubactivitiesDialog::~SubactivitiesDialog()
{
    delete ui;
}

void SubactivitiesDialog::on_pushButton_clicked()
{
    mainView view;

    QString old_value = ui->comboBox->currentText();

    qDebug() << old_value;
    QString new_value = QInputDialog::getText(this, "Input", "Old value = " + old_value + ". Write the new value");
    if (new_value != "") {
        view.modify_subproject("activities", file_act3, "code", old_value, new_value, code);
    }
}

