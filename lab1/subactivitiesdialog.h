#ifndef SUBACTIVITIESDIALOG_H
#define SUBACTIVITIESDIALOG_H

#include <QDialog>

namespace Ui {
class SubactivitiesDialog;
}

class SubactivitiesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SubactivitiesDialog(QWidget *parent = nullptr);
    ~SubactivitiesDialog();

    void set_code(QString);

    void initialize(QString);


private slots:
    void on_pushButton_clicked();

private:
    Ui::SubactivitiesDialog *ui;
};

#endif // SUBACTIVITIESDIALOG_H
