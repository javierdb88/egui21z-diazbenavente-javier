#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mainview.h"
#include "entrydialog.h"
#include "activitydialog.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_username_lineedit_editingFinished()
{
    mainView main_view(this);
    EntryDialog entry;
    ActivityDialog act;

    entry.set_username_entry(ui->username_lineedit->text());
    act.set_file_act(QDir::currentPath() + "/activity.json");

    main_view.set_username(ui->username_lineedit->text());

    hide();
    QMessageBox::information(this, "Welcome", "Welcome, " + ui->username_lineedit->text());
    main_view.exec();

}

