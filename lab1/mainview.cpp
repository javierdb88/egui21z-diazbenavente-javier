#include "mainview.h"
#include "ui_mainview.h"
#include "entrydialog.h"
#include "activitydialog.h"
#include "subactivitiesdialog.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QFile>
#include <QInputDialog>


QString user;
QString file_act = QDir::currentPath() + "/activity.json";
QString file_en = QDir::currentPath() + "/entry.json";




void mainView::set_username(QString userName) {

    user = userName;

}


void mainView::set_file_actMain(QString file) {                                                 //setter for file_act

    file_act = file;

}

mainView::mainView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::mainView)
{
    ui->setupUi(this);


    ActivityDialog act;
    QJsonDocument doc = act.readJson(file_act);
    QStringList codes = act.find_attributes_activities(doc, "Code");

    QStringList subcodes = act.find_subcodes(doc);

    ui->edit_project_combobox->addItems(codes);
    ui->edit_activity_combobox->addItems(subcodes);

    QStringList attributes_project;
    QStringList attributes_activity;

    attributes_project.append("Code");
    attributes_project.append("Manager");
    attributes_project.append("Name");
    attributes_project.append("Budget");
    attributes_project.append("Active");
    attributes_project.append("Subprojects");

    attributes_activity.append("Date");
    attributes_activity.append("Time");
    attributes_activity.append("Description");

    ui->attr_project_combobox->addItems(attributes_project);
    ui->attr_activity_combobox->addItems(attributes_activity);


}

mainView::~mainView()
{
    delete ui;
}


void mainView::modify_attribute_project(QString file_name, QString filePath, QString attr, QString old_value, QString new_value) {              // modifies the value of the attribute selected, assigning the parameter new_value

    QFile file(filePath);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    file.close();

    ActivityDialog act;

    QJsonObject obj = doc.object();
    QJsonValueRef ref = obj.find(file_name).value();
    QJsonArray ref_array = ref.toArray();

    int index;
    if (file_name == "activities") {
        index = act.get_index(doc, attr, old_value);
    } else {
        index = act.get_index_entry(doc, attr, old_value);
    }


    QJsonObject ref_obj = ref_array[index].toObject();

    ref_obj.insert(attr, new_value);                                                 // set the value we want to modify

    ref_array.insert(index, ref_obj);
    ref_array.removeAt(index+1);
    ref = ref_array;                                                                  //assign the modified object to reference
    doc.setObject(obj);                                                             // set to JSON document


    file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
    file.write(doc.toJson());
    file.close();

    QMessageBox::information(this, "Modified", "Attribute modified succesfully");

}


void mainView::modify_int_attribute_project(QString file_name, QString filePath, QString attr, int old_value, int new_value) {              // modifies the (int) value of an attribute

    QFile file(filePath);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    file.close();

    ActivityDialog act;

    QJsonObject obj = doc.object();
    QJsonValueRef ref = obj.find(file_name).value();
    QJsonArray ref_array = ref.toArray();
    int index = act.get_index_int(file_name, doc, attr, old_value);
    QJsonObject ref_obj = ref_array[index].toObject();
    ref_obj.insert(attr, new_value);                                                 // set the value we want to modify
    ref_array.insert(index, ref_obj);
    ref_array.removeAt(index+1);
    ref = ref_array;                                                                  //assign the modified object to reference
    doc.setObject(obj);                                                             // set to JSON document

    file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
    file.write(doc.toJson());
    file.close();

    QMessageBox::information(this, "Modified", "Attribute modified succesfully");

}



void mainView::modify_subproject(QString file_name, QString filePath, QString attr, QString old_value, QString new_value, QString old_code) {               // modifies the attribute of a subproject

    QFile file(filePath);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    file.close();

    ActivityDialog act;

    QJsonObject obj = doc.object();
    QJsonValueRef ref = obj.find(file_name).value();
    QJsonArray ref_array = ref.toArray();


    int index;
    if (file_name == "activities") {
        index = act.get_index(doc, "Code", old_code);
    } else {
        index = act.get_index_entry(doc, "Code", old_code);
    }


    QJsonObject ref_obj = ref_array[index].toObject();
    QJsonValueRef ref_attr = ref_obj.find("Subactivities").value();
    QJsonArray ref_array2 = ref_attr.toArray();
    int index2;
    QJsonObject index_obj;

    for (int i = 0; i < ref_array2.size(); i++) {

        index_obj = ref_array2[i].toObject();
        QJsonValueRef ref_attr2 = index_obj.find("code").value();

        if (ref_attr2.toString() == old_value) {                                            // if the value value found is the same as the parameter 'attr_value', we return the index
            index2 = i;
            break;

        } else { index2 = -1; }

    }

    QJsonObject index_obj2 = ref_array2[index2].toObject();
    index_obj.insert(attr, new_value);                                                 // set the value we want to modify
    ref_array2.insert(index2, index_obj);
    ref_array2.removeAt(index2+1);
    ref_obj.insert("Subactivities", ref_array2);
    ref_array.insert(index, ref_obj);
    ref_array.removeAt(index+1);
    ref = ref_array;                                                                  //assign the modified object to reference
    doc.setObject(obj);                                                             // set to JSON document


    file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
    file.write(doc.toJson());
    file.close();

    QMessageBox::information(this, "Modified", "Attribute modified succesfully");

}


void mainView::on_AddButton_2_clicked()
{
    EntryDialog entryDialog(this);                          // we create an objet entryDialog
    entryDialog.setModal(true);                             // we call the method setModal so that the user can't access to mainWindow while entryDialog is active
    entryDialog.exec();
}


void mainView::on_add_activity_button_clicked()
{
    ActivityDialog activityDialog(this);                        // we create an objet activityDialog
    activityDialog.setModal(true);                             // we call the method setModal so that the user can't access to mainWindow while entryDialog is active

    activityDialog.exec();
}


void mainView::on_edit_project_button_clicked()
{
    ActivityDialog act;
    QFile file(file_act);
    QJsonDocument doc = act.readJson(file_act);

    if (ui->attr_project_combobox->currentText() == "Code") {

        int index = act.get_index(doc, "Code", ui->edit_project_combobox->currentText());
        QString old_value = act.find_attribute_index(doc, index, "Code");
        QString new_value = QInputDialog::getText(this, "Input", "Old value = " + old_value + ". Write the new value");
        if (new_value != "") {
            modify_attribute_project("activities", file_act, "Code", old_value, new_value);
            ui->edit_project_combobox->setItemText(index, new_value);

        }
    }

    if (ui->attr_project_combobox->currentText() == "Manager") {

        QMessageBox::warning(this, "Warning", "Manager cannot be modified");
    }

    if (ui->attr_project_combobox->currentText() == "Name") {

        int index = act.get_index(doc, "Code", ui->edit_project_combobox->currentText());
        QString old_value = act.find_attribute_index(doc, index, "Name");
        QString new_value = QInputDialog::getText(this, "Input", "Old value = " + old_value + ". Write the new value");
        if (new_value != "") {
            modify_attribute_project("activities", file_act, "Name", old_value, new_value);

        }
    }

    if (ui->attr_project_combobox->currentText() == "Budget") {                     // REVISAR casting

        int index = act.get_index(doc, "Code", ui->edit_project_combobox->currentText());
        int old_value = act.find_attribute_index_int(doc, index, "Budget", "activities");
        int new_value = QInputDialog::getInt(this, "Input", "Old value = " + QString::number(old_value) + ". Write the new value");

        if (new_value!=NULL) {
            modify_int_attribute_project("activities", file_act, "Budget", old_value, new_value);
        }


    }

    if (ui->attr_project_combobox->currentText() == "Active") {

        int index = act.get_index(doc, "Code", ui->edit_project_combobox->currentText());
        QString old_value = act.find_attribute_index(doc, index, "Active");
        QString new_value = QInputDialog::getText(this, "Input", "Old value = " + old_value + ". Write the new value");
        if (new_value != "true" && new_value != "false") {
            QMessageBox::warning(this, "Warning", "The value for 'Active' can only be 'true' or 'false'. Try again.");

        }
        else if (new_value != "") {
            modify_attribute_project("activities", file_act, "Active", old_value, new_value);

        }
    }

    if (ui->attr_project_combobox->currentText() == "Subprojects") {

        int index = act.get_index(doc, "Code", ui->edit_project_combobox->currentText());
        QString code = ui->edit_project_combobox->currentText();

        QString option = QInputDialog::getText(this, "Input", "Do you want to add(a) a new subactivity or to edit(e) an existing one?");
        QJsonObject obj = doc.object();
        QJsonObject obj2;
        QJsonValueRef ref = obj.find("activities").value();
        QJsonArray activities = ref.toArray();
        QJsonObject index_obj = activities[index].toObject();
        QJsonArray activities_copy = activities;
        activities_copy.append(obj2);
        QJsonValueRef ref_attr = index_obj.find("Subactivities").value();
        QJsonArray subactivities = ref_attr.toArray();
        QJsonArray subactivities_copy = subactivities;


        if ((option == "a") | (option == "add")) {

            QString name = QInputDialog::getText(this, "Input", "Enter the name of the new  subactivity");
            if (name != "") {
                QJsonObject obj_sub;
                obj_sub["code"] = name;
                subactivities_copy.append(obj_sub);
                index_obj["Subactivities"] = subactivities_copy;

                activities.removeAt(index);
                activities.insert(index, index_obj);
                obj["activities"] = activities;
                doc.setObject(obj);
                act.saveJson(doc, file_act);
            }

        }

        else if ((option == "e") | (option == "edit")) {

            SubactivitiesDialog sub(this);

            sub.set_code(code);

            sub.setModal(true);
            sub.exec();

        }

    }
}


void mainView::on_edit_activity_button_clicked()
{

    ActivityDialog act;
    QFile file(file_en);
    QJsonDocument doc = act.readJson(file_en);

    if (ui->attr_activity_combobox->currentText() == "Date") {

        int index = act.get_index_entry(doc, "subcode", ui->edit_activity_combobox->currentText());
        QString old_value = act.find_attribute_index_entry(doc, index, "date");

        QString new_value = QInputDialog::getText(this, "Input", "Old value = " + old_value + ". Write the new value");
        if (new_value != "") {
            modify_attribute_project("entries", file_en, "date", old_value, new_value);

        }
    }

    if (ui->attr_activity_combobox->currentText() == "Time") {

        QJsonDocument doc2 = act.readJson(file_act);

        int index = act.get_index_entry(doc, "subcode", ui->edit_activity_combobox->currentText());
        int code_index = act.get_index_entry(doc, "Code", ui->edit_project_combobox->currentText());
        QString active = act.find_attribute_index(doc2, code_index, "Active");
        if (active == "false") {

            QMessageBox::warning(this, "Warning", "You cannot modify time for this activity because the project is not active");
        }
        else {
            QString old_value = act.find_attribute_index_entry(doc, index, "time");

            int new_value = QInputDialog::getInt(this, "Input", "Old value = " + old_value + ". Write the new value");
            if (new_value != NULL) {
                modify_attribute_project("entries", file_en, "time", old_value, QString::number(new_value));
            }
        }
    }

    if (ui->attr_activity_combobox->currentText() == "Description") {

        int index = act.get_index_entry(doc, "subcode", ui->edit_activity_combobox->currentText());
        QString old_value = act.find_attribute_index_entry(doc, index, "description");

        QString new_value = QInputDialog::getText(this, "Input", "Old value = " + old_value + ". Write the new value");

        if (new_value != "") {
            modify_attribute_project("entries", file_en, "description", old_value, new_value);

        }
    }



}

void mainView::on_delete_activity_button_clicked()
{

    ActivityDialog act;
    QJsonDocument doc = act.readJson(file_en);
    QJsonObject obj = doc.object();
    QJsonValueRef ref = obj.find("entries").value();
    QJsonArray entries = ref.toArray();


    int index = act.get_index_entry(doc, "subcode", ui->edit_activity_combobox->currentText());


    entries.removeAt(index);
    ref = entries;
    doc.setObject(obj);

    QByteArray data_json = doc.toJson();
    QFile output(file_en);
    if (output.open(QIODevice::WriteOnly | QIODevice::Text)) {
        output.write(data_json);
        output.close();
        QMessageBox::information(this, tr("Deleted"), tr("Activity deleted succesfully"));
    } else {
        QMessageBox::critical(this, tr("Error"), output.errorString());
    }

}

